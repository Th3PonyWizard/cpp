#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
using namespace std;
void read ();
void write();
int numofreads = 0;
int numofwrites = 0;
bool ok = false;
mutex mutmut;
condition_variable concon;
int main() {
    thread t1(read);
    thread t2(write);
    t1.join();
    t2.join();
    return 0;
}
void read () {
unique_lock<mutex> lock(mutmut, defer_lock);
numofreads++;
lock.lock();
if(ok) {
    concon.wait(lock, []{return (!ok);});
}
cout << "Reading!\n" << endl;
this_thread::sleep_for(chrono::milliseconds(24));
ok = true;
cout << "Reading's over\n" << endl;
numofreads--;
lock.unlock();
concon.notify_one();
}
void write() {
unique_lock<mutex> lock(mutmut, defer_lock);
numofwrites++;
lock.lock();
cout << "Writing!\n" << endl;
this_thread::sleep_for(chrono::milliseconds(24));
if(ok) {
    ok = false;
}
cout << "Writing's over\n" << endl;
numofwrites--;
lock.unlock();
concon.notify_one();
}
